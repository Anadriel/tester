import java.time.temporal.ChronoUnit
import akka.actor.{Actor, ActorLogging}

class Counter extends Actor with ActorLogging{
  var counterAll: Long = 0L
  var counterSucceeded: Long = 0L
  var counterFailed: Long = 0L

  var totalAll: Long = 0L
  var totalSucceeded: Long = 0L
  var totalFailed: Long = 0L

  var last: java.time.ZonedDateTime = java.time.ZonedDateTime.now

  def receive: Receive = {
    case IncSuccess =>
      counterSucceeded += 1
      counterAll += 1
    case IncFailure =>
      counterFailed += 1
      counterAll += 1
    case Show =>
      val now = java.time.ZonedDateTime.now
      val elapsed = ChronoUnit.SECONDS.between(last, now)
      last = now
      totalAll += counterAll
      totalSucceeded += counterSucceeded
      totalFailed += counterFailed
      log.info(s"Total: all: $totalAll, success: $totalSucceeded, failure: $totalFailed; since last: all: $counterAll , success: $counterSucceeded, failure: $counterFailed; throughput: ${counterAll / elapsed} m/s")
      counterAll = 0
      counterSucceeded = 0
      counterFailed = 0
    case Shutdown =>
      val now = java.time.ZonedDateTime.now
      val elapsed = ChronoUnit.SECONDS.between(last, now)
      totalAll += counterAll
      totalSucceeded += counterSucceeded
      totalFailed += counterFailed
      log.info(s"Before shutdown: \n Total: all: $totalAll, success: $totalSucceeded, failure: $totalFailed; since last: all: $counterAll , success: $counterSucceeded, failure: $counterFailed; throughput: ${counterAll / elapsed} m/s")
      context.system.terminate
      ()
  }
}
