import com.typesafe.config.ConfigFactory

trait Config {
  protected val config = ConfigFactory.load()

  protected lazy val interface = config.getString("http.interface")
  protected lazy val port = config.getInt("http.port")

  protected lazy val testedServiceHost = config.getString("services.tested.host")
  protected lazy val testedServicePort = config.getInt("services.tested.port")

}
