name := "tester"

organization := "com.brabdev"

version := "1.0"

scalaVersion := "2.11.8"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8", "-Yrangepos", "-language:postfixOps")

val akkaV = "2.4.6"
val slickV = "3.1.1"
val akkaGroupId = "com.typesafe.akka"

libraryDependencies ++= Seq(
  akkaGroupId %% "akka-actor" % akkaV,
  akkaGroupId %% "akka-stream" % akkaV,
  akkaGroupId %% "akka-http-core" % akkaV,
  akkaGroupId %% "akka-http-spray-json-experimental" % akkaV,
  "org.scalatest" %% "scalatest" % "3.0.0-RC1" % Test,
  akkaGroupId %% "akka-http-testkit" % akkaV % Test
)

assemblyJarName in assembly := "optionarium.jar"

test in assembly := {}