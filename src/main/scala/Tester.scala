import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.util.Timeout
import java.util.concurrent.Executors

import akka.event.Logging
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import spray.json.DefaultJsonProtocol

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

object Tester extends App with DefaultJsonProtocol with Config {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val dispatcher = ExecutionContext.fromExecutor(Executors.newCachedThreadPool)
  implicit val timeout = Timeout(5.seconds)

  val counter = system.actorOf(Props[Counter])
  val sender = system.actorOf(Props(new Sender(counter)))

  lazy val logger = Logging(system, getClass)

  val routes = {
    logRequestResult("tester") {
      path("start"/Segment/Segment) { (am, n) =>
        complete {
          (0L to am.toLong - 1).foreach{ x =>
            logger.debug(s"Iteration $x")
            (1 to n.toInt).foreach(sender ! Request(_))
          }
          OK
        }
      } ~
        path("stop"){
          complete{
            counter ! Shutdown
            OK
          }
        } ~
        path("show"){
          complete{
            counter ! Show
            OK
          }
        }
    }
  }

  Http().bindAndHandle(routes, interface, port)

}

