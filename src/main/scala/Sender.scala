import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.model.StatusCodes.Success
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Sink, Source}

import scala.concurrent.{ExecutionContext, Future}

class Sender(counter: ActorRef)(implicit system: ActorSystem, materializer: ActorMaterializer, dispatcher: ExecutionContext) extends Actor with ActorLogging with Config {

  lazy val connectionFlow: Flow[HttpRequest, HttpResponse, Any] = Http().outgoingConnection(testedServiceHost, testedServicePort)

  private def requestTestedService(request: HttpRequest): Future[HttpResponse] = {
    Source.single(request).via(connectionFlow).runWith(Sink.head)
  }

  def makeRequest(id: Long) = {
    requestTestedService(RequestBuilding.Get(s"/bet/$id")).map { response =>
      response.status match {
        case Success(_) =>
          counter ! IncSuccess
          val resp = Unmarshal(response.entity)
          log.debug(s"Got success result ${resp.value}")
        case e =>
          counter ! IncFailure
          log.error(s"Got not success result with code ${e.defaultMessage}")
      }
    }
  }

  def receive: Receive = {
    case Request(id) =>
      makeRequest(id)
  }
}
