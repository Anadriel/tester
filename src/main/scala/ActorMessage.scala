
sealed trait ActorMessage

case object IncSuccess extends ActorMessage
case object IncFailure extends ActorMessage
case object Show extends ActorMessage
case object Shutdown extends ActorMessage
case class Request(id: Int) extends ActorMessage
